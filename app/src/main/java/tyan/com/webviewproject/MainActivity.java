package tyan.com.webviewproject;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;

public class MainActivity extends AppCompatActivity implements LoadFinisher {

    private WebView mWebView;
    private SwipeRefreshLayout mRefresh;
    private ProgressBar mProgressBar;
    public static final String URL = "http://aut24.ru/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //включение прогрессбара
        //todo вмсто прогрессбара можно вставить какой-нибудь прелоадер
        mProgressBar = findViewById(R.id.progress_bar_id);
        if (null != mProgressBar) {
            mProgressBar.setVisibility(View.VISIBLE);
        }

        mWebView = findViewById(R.id.webView);
        if (null != mWebView) {
            try {
                //поддержка JavaScript
                //mWebView.getSettings().setJavaScriptEnabled(true);

                //страница загрузки
                mWebView.loadUrl(URL);

                //что бы загружалось в окне приложения, а не открывался браузер
                mWebView.setWebViewClient(new MWebViewClient(this));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        mRefresh = findViewById(R.id.swipe_refresh_layout_id);
        if (null != mRefresh) {
            mRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    Log.d("myLog", "-refresh-");
                    update();
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.refresh_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_refresh:
                Log.d("myLog", "-refresh menu-");

                // Signal SwipeRefreshLayout to start the progress indicator
                mRefresh.setRefreshing(true);

                // Start the refresh background task.
                // This method calls setRefreshing(false) when it's finished.
                update();

                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private void update() {
        try {
            Log.d("myLog", "-update-");
            mWebView.reload();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onLoadFinish() {
        try {
            mProgressBar.setVisibility(View.GONE);
            mRefresh.setRefreshing(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}