package tyan.com.webviewproject;

public interface LoadFinisher {
    void onLoadFinish();
}
