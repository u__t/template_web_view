package tyan.com.webviewproject;

import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MWebViewClient extends WebViewClient {

    private LoadFinisher mDelegate;

    MWebViewClient(LoadFinisher delegate){
        mDelegate = delegate;
    }

    //возвращать true если нужно что бы страница открвалась в приложении, если вернуть false, то
    //откроется в браузере.
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        try {
            if (null != mDelegate) {
                mDelegate.onLoadFinish();
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
